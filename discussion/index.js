// DIFFERENCE
/* 
    1. Quotation Marks
    JS Objects only has the value inserted inside quotation marks.
    JSON has the quotation marks for both key and the value

    2.
    JS objects exclusive to javascript.
    JSON not exclusive for javascript. other programming languages can also use JSON files
*/
/* 
    create and log a JS object with the following properties
        city
        province
        country
*/

/* const address = {
    city: "San Juan",
    province: "Metro Manila",
    country: "Philippines"
};

console.log(address) */

// JSON Object
/* 
    JavaScript Object Notation
    JSON - used for serializing/deserializing different data types into byte
        Serialization - process of converting data into series of bytes for easier transfer of info
*/
const address = {
    "city": "San Juan",
    "province": "Metro Manila",
    "country": "Philippines"
};

console.log(address);

// JSON Arrays
const cities = [
    {
        "city": "San Juan",
        "province": "Metro Manila",
        "country": "Philippines"
    },
    {
        "city": "Bacoor",
        "province": "Cavite",
        "country": "Philippines"
    },
    {
        "city": "New York",
        "province": "New York",
        "country": "U.S.A."
    }
];

console.log(cities);

// JSON Methods

    // JSON object contains methods for parsing and converting data into stringified JSON
    // Stringified JSON - JSON Object converted into string to be used in other functions of the language es.p javascript-based apps (serialize)


let batches = [
    {
        "batchName": "Batch X"
    },
    {
        "batchName": "Batch Y"
    }
];

console.log("Result from console.log method");
console.log(batches);

console.log("Result from stringify method");
console.log(JSON.stringify(batches));

let data = JSON.stringify({
    name:"John",
    age: 31,
    address:{
        city:"Manila",
        country:"Philippines"
    }
});
console.log(data);

// const userDetails = JSON.stringify(
//     {
//         name: prompt("Please input your first name."),
//         lname: prompt("Please input your last name."),
//         age: prompt("Please input your age."),
//         address:{
//             city: prompt("City"),
//             country: prompt("Country"),
//             zipCode: prompt("Zip Code")
//         }
//     }
// );

// console.log(userDetails);

let batchesJSON = `[
    {
        "batchName": "Batch X"
    },
    {
        "batchName": "Batch Y"
    }
]`;
console.log(batchesJSON);
console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedData = `{
    "name": "John",
    "age": "31",
    "address":{
        "city": "Manila",
        "country": "Philippines"
    }
}`;
console.log(JSON.parse(stringifiedData))